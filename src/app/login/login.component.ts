import { Component, OnInit } from '@angular/core';
import {UserApiService} from '../services/user-api.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email:string;
  password: string;
  error: string;

  constructor(private api: UserApiService, private router: Router) { }

  ngOnInit(): void {

    this.api.check().subscribe(res => {

      this.router.navigate(['/home'], { replaceUrl: true });
     
    });
    

  }

  login()
  {
    console.log(this.email);
    console.log(this.password);
    this.api.login(this.email, this.password).subscribe(res =>{
      localStorage.setItem("token", res["token"]);
      this.router.navigate(['/home'], { replaceUrl: true });
    }, 
    error => {
      this.error = "Please check your credentials"
      console.log(error);
    }
    )
  }


}
