import { Component, OnInit } from '@angular/core';
import {UserApiService} from '../services/user-api.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  first_name: string;
  last_name: string;


  constructor(private api: UserApiService, private router: Router) { }

  ngOnInit(): void {

    this.api.showUser().subscribe(res => {
      this.first_name = res["first_name"];
      this.last_name = res["last_name"];
    })
  }

  logout()
  {
    this.api.logout().subscribe(res => {
      localStorage.removeItem("token");
      this.router.navigate(['/login'], { replaceUrl: true });
    }, error =>{
      localStorage.removeItem("token");
      this.router.navigate(['/login'], { replaceUrl: true });
    })
  }

}
