import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuardGuard} from './services/auth-guard.guard'

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', 
   loadChildren:() => import('./home/home.module').then(m => m.HomeModule),
   canActivate: [AuthGuardGuard]
  
},
  {path: "login", loadChildren:() => import("./login/login.module").then(m => m.LoginModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
