import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserApiService {

  constructor(private http: HttpClient) { }
  apiUrl:string = "http://localhost:8005";

  login(email: string, password: string)
  {
    const body = new FormData();
    body.append("email", email);
    body.append("password", password);

    const headers= {'Content-Type': 'application/x-www-form-urlencoded'};

    return this.http.post(this.apiUrl+"/login", body);
  }

  check()
  {
    const token =localStorage.getItem("token");
    return this.http.get(this.apiUrl+"/check", { headers: { Authorization: "Bearer "+ token } });
    

  }

  logout()
  {
    const token =localStorage.getItem("token");
    return this.http.get(this.apiUrl+"/logout", { headers: { Authorization: "Bearer "+ token } });
  }

  showUser()
  {
      const token = localStorage.getItem("token");
      return this.http.get(this.apiUrl+"/user", { headers: { Authorization: "Bearer "+ token } })
  }


}
