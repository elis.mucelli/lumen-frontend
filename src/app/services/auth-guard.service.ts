import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { UserApiService } from './user-api.service';
import { Router } from '@angular/router';


@Injectable()
export class AuthGuardService {

  constructor(private api: UserApiService, private router: Router) {}


  /**
   * Checks is the user is authenticated.
   * @return {boolean} True if the user is authenticated.
   */
  async isAuthenticated(): Promise<boolean> {

    let bool = false;
    await this.api.check().subscribe(
      suc => {
        console.log(suc);
        bool = true;
        return true;
      },
      err => {
        bool = false;
        this.router.navigate(['/login']);
        console.log(err);
        return false;
      }
    );
    
    return bool;
  }
}
